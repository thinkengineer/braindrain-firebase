/* Firebase */
import * as functions from 'firebase-functions';
import { DataSnapshot } from 'firebase-functions/lib/providers/database'
import { QueryDocumentSnapshot, QuerySnapshot, DocumentData } from '@google-cloud/firestore';
import { TextEncoder } from 'text-encoding';
const admin = require('firebase-admin')

/* Response imports */
const moment = require('moment')
const cors = require('cors')({
  origin: true,
})

/* Initialise Firebase */
admin.initializeApp(functions.config().firebase)

/**
 * Device API.
 */
exports.api = functions.https.onRequest((req, res) => {
    
    /* Not POST */
    if (req.method !== 'POST') {

        /* 403 */
        return cors(req, res, () => {
            res.status(403).send('Forbidden!')
        })

    } 

    /* POST */
    else {

        /* 200 */
        return cors(req, res, () => {

            /* Get header information */
            let app: string = req.body.app_id
            let device: string = req.body.dev_id
            let time = req.body.metadata.time
    
            /* Get header authorisation */
            let requestAuth: string = req.headers.authorization
            //console.log(`Authorisation: ${requestAuth}`)

            /* Get path to authorisation baed on app_id */
            let authPath: string = `/authorisation/${app}`
            //console.log(`Pulling ${authPath}`)
    
            /* Pull stored authorisation */
            admin.firestore()
                .doc(authPath)
                .get().then((snapshot: QueryDocumentSnapshot)  => {
                    let storedAuth: string = snapshot.get('key')

                    /* Incorrect key */
                    if (storedAuth !== requestAuth) {
                        res.status(403).send('Forbidden!')
                    } 
                    
                    /* Correct key */
                    else {

                        /* Add data */
                        admin.firestore()
                            .collection('devices')
                            .doc(app)
                            .collection(device)
                            .add({
                                timestamp: new Date(time),
                                button: req.body.payload_fields.button,
                                distance: req.body.payload_fields.distance
                            })

                        res.status(200).send('Success!')

                    }

            })
            
        })

    }
    
})

exports.getData = functions.https.onRequest((req, res) => {

    return cors(req, res, () => {
        admin.firestore()
        .collection(`devices/${req.query.app}/${req.query.device}`)
        .where('timestamp', '>', new Date(Date.now() - 60000 * 5))
        .get().then((doc: QuerySnapshot) => {

            let data: Array<DocumentData> = new Array
            doc.forEach((result: QueryDocumentSnapshot) => {
                data.push(result.data());
            })
            data.sort((a: DocumentData, b: DocumentData) => {
                return (a.timestamp < b.timestamp) ? -1 : (a.timestamp > b.timestamp) ? 1 : 0
            })
            //console.log(data)

            res.status(200).send(data)
        })
    })

})