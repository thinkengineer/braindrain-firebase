"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* Firebase */
const functions = require("firebase-functions");
const admin = require('firebase-admin');
/* Response imports */
const moment = require('moment');
const cors = require('cors')({
    origin: true,
});
/* Initialise Firebase */
admin.initializeApp(functions.config().firebase);
/**
 * Device API.
 */
exports.api = functions.https.onRequest((req, res) => {
    /* Not POST */
    if (req.method !== 'POST') {
        /* 403 */
        return cors(req, res, () => {
            res.status(403).send('Forbidden!');
        });
    }
    /* POST */
    else {
        /* 200 */
        return cors(req, res, () => {
            /* Get header information */
            let app = req.body.app_id;
            let device = req.body.dev_id;
            let time = req.body.metadata.time;
            /* Get header authorisation */
            let requestAuth = req.headers.authorization;
            //console.log(`Authorisation: ${requestAuth}`)
            /* Get path to authorisation baed on app_id */
            let authPath = `/authorisation/${app}`;
            //console.log(`Pulling ${authPath}`)
            /* Pull stored authorisation */
            admin.firestore()
                .doc(authPath)
                .get().then((snapshot) => {
                let storedAuth = snapshot.get('key');
                /* Incorrect key */
                if (storedAuth !== requestAuth) {
                    res.status(403).send('Forbidden!');
                }
                /* Correct key */
                else {
                    /* Add data */
                    admin.firestore()
                        .collection('devices')
                        .doc(app)
                        .collection(device)
                        .add({
                        timestamp: new Date(time),
                        button: req.body.payload_fields.button,
                        distance: req.body.payload_fields.distance
                    });
                    res.status(200).send('Success!');
                }
            });
        });
    }
});
exports.getData = functions.https.onRequest((req, res) => {
    return cors(req, res, () => {
        admin.firestore()
            .collection(`devices/${req.query.app}/${req.query.device}`)
            .where('timestamp', '>', new Date(Date.now() - 60000 * 5))
            .get().then((doc) => {
            let data = new Array;
            doc.forEach((result) => {
                data.push(result.data());
            });
            data.sort((a, b) => {
                return (a.timestamp < b.timestamp) ? -1 : (a.timestamp > b.timestamp) ? 1 : 0;
            });
            //console.log(data)
            res.status(200).send(data);
        });
    });
});
//# sourceMappingURL=index.js.map